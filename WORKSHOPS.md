# Selected Schools and Workshops

- **12th Summer Course on Computational Neuroscience**, MPI-DS
Göttingen, _2014_
- **Workshop on Advanced Methods in Theoretical Neuroscience**,
MPI-DS Göttingen, _2017_
- **VIII Gefenol Summer School on Statistical Physics of Complex
and Small Systems**, IFISC Palma de Mallorca, _2018_
- **Stochastic Dynamics on Large Networks: Prediction and Inference**, MPI-PKS Dresden, _2018_
- **Workshop on Advanced Methods in Theoretical Neuroscience**,
MPI-DS Göttingen, _2019_
- **Neuromatch Academy**, (online), _2020_
- **Fred Kavli Summer School on Mathematical Methods in
Computational Neuroscience**, (online), _2021_
- **Theoretical and Computational Neuroscience Summer School
(CNeuro)**, (online), _2021_
- **Recent Advances in Understanding Artificial and Biological Neural
Networks**, Les Houches School of Physics, _2023_
