# Past teaching experience

- [Network Dynamics & Complex Systems](https://dimitra-maoutsa.gitlab.io/lecture-network-dynamics-and-complex-systems)
- [Data Science course](https://gitlab.com/di.ma/data-science-course)  - GGNB methods course (May 8th-12th 2017) 
    _(can be accessed only once you log in your gitlab account)_
