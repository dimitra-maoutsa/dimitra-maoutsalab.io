# Summary

* [Research Projects](README.md)
* [Teaching](TEACHING.md)
* [Publications](PUBLICATIONS.md)
* [Workshops & Schools](WORKSHOPS.md)
