# Dimitra Maoutsa  [<img src="images/blog_log.png"  align="right" width="3.8%" height="3.8%">](https://dimitra-maoutsa.github.io/M-Dims-Blog/)   [<img src="images/mastodon-icon-512x512-fl0fay7j.png"  align="right" width="4%" height="4%" rel="me">](https://scholar.social/@_dim_ma_) [<img src="images/twitter.png"  align="right" width="5%" height="5%">](https://twitter.com/_dim_ma_) [<img src="images/github.png"  align="right" width="4%" height="4%">](https://github.com/dimitra-maoutsa) [<img src="images/orcid_logo.jpg"  align="right" width="6%" height="6%">](https://orcid.org/0000-0002-3553-8658) [<img src="images/google_schol.png"  align="right" width="4.5%" height="4.5%">](https://scholar.google.com/citations?user=yAsW_icAAAAJ&hl=en) 


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5C8JS2N');</script>
<!-- End Google Tag Manager -->



<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5C8JS2N"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

- [**Revealing latent stochastic dynamics from single-trial spike train observations**](https://gitlab.com/dimitra-maoutsa/revealing_latent_stochastic_dyanmics_from_spikes/-/tree/main) 

<div align="center">   
   <img src="intro.png" alt="latent dynamics inference by dimitra maoutsa" width="75%" height="75%"> 
   </div>

[work in progress...] We are developing a framework for identifying latent stochastic dynamics from spike train observations. We reformulate the inference in terms of stochastic control and employ the [deterministic particle control framework](https://dimitra-maoutsa.gitlab.io/probability-flow-dynamics-for-constrained-stochastic-nonlinear-systems) to sample the unobserved path distribution.

- [**Identifying stochastic systems from discrete time observations**](https://dimitra-maoutsa.gitlab.io/inferring-stochastic-systems-from-discrete-time-observations) 

<div align="center">   
   <img src="images/metric.png" alt="Riemannian metric induced by the observations by dimitra maoutsa" width="35%" height="35%"> 
   </div>

   **Reconciling geometric and path-space approaches for identifying diffusion processes.**

   We employed insights from nonlinear dynamics, Riemannian geometry, and stochastic analysis to device a novel path augmentation framework that allows for efficient and accurate inference of stochastic nonlinear systems.


- [**Probability flow dynamics for constrained stochastic systems**](https://dimitra-maoutsa.gitlab.io/probability-flow-dynamics-for-constrained-stochastic-nonlinear-systems) 
   <div align="center">
   <img src="images/waterfall_plot_cmap25bc.png" alt="schematic for deterministic particle flow control by dimitra maoutsa" width="40%" height="40%"> 
   <img src="images/landscape_r.png" alt="phenotypic landscape by dimitra maoutsa" width="35%" height="35%"> 
   </div>
   
- [**ODEs for SDEs**](https://gitlab.com/dimitra-maoutsa/odes_for_sdes/-/blob/master/README.md)
   <div align="center">
   <img src="images/Fokker_plot.png" alt="comparison of stochastic and deterministic particle systems in approximating the transient Fokker-Planck solution of a bistable system by dimitra maoutsa" width="70%" height="70%"> 
   </div>
   
   


   <div align="center">
   <img src="images/OU2Dc.png" alt="stochastic and deterministic particle approximation of evolving pdf of a stochastic system by dimitra maoutsa" width="20%" height="20%"> 
   </div>




   **A computational method for fast and efficient simulation of transient Fokker--Planck equation solutions.**

   We introduced an **interacting particle system** with purely deterministic dynamics that provides accurate  Fokker--Planck solutions for diffusive systems. The formulation relies on a **variational representation of the logarithmic gradient** of the instaneous Fokker-Planck probability density. Our approach does not require the knowledge (or even the existence) of a steady state, and is thereby relevant even for systems with time inhomogeneous dynamics.






-  [**Inferring synaptic interactions and transmission delays**](https://dimitra-maoutsa.gitlab.io/inferring-synaptic-interactions-and-transmission-delays)
   
   <div align="center">
   <img src="images/event_space.png" alt="mapping events to event space by dimitra maoutsa" width="50%" height="50%">
   <img src="images/linearisation.png" alt="local linear approximation by dimitra maoutsa" width="25%" height="25%">
   <img src="images/images_inferring_delays.png" alt="inferring connectivity and transmission delays among neurons by dimitra maoutsa" width="50%" height="50%">
   </div>

   **Inference method for identifying synaptic interactions and transmission delays from spiking activity.**

    We proposed a mapping of the spiking activity to high-dimensional spaces, called **_event spaces_**, spanned by the inter-spike intervals of a selected neuron and the respective cross-spike intervals of the rest of the network. The mapping from the raster plot to the event spaces may be viewed as a **sampling of the inter-spike interval generating function** for each neuron. To identify the effect of putative pre-synaptic neurons to a post-synaptic one we proposed a **linearisation** of the inter-spike interval generating function around a reference point (reference event).   
   To identify the **transmission delays**, we proposed the minimisation of the approximation error in the space of interaction delays. To speed up the optimisation and avoid local minima, we optimised the delays on a **smoothed error landscape** approximated by radial basis function interpolation.
   

- [**Perturbing spiking neural networks with structured connectivity**](https://github.com/dimitra-maoutsa/Perturbing_oscillatory_spiking_neural_networks)

   **Can one hear the shape of a network?**

   Small project on spreading perturbations on spiking neural networks. 
   I found that the **relaxation** of the network activity **to an attracting collective reference state** may reveal 
   global  properties of the underlying network structure in terms of the **first few eigenvectors** of the **connectivity 
   matrix**.


- [**Phase transitions of autonomous intersection traffic**](https://dimitra-maoutsa.gitlab.io/autonomous-intersection-traffic) 
   <div align="center">
   <img src="images/intersection_schematic.png" alt="schematic of an autonomous intersection by dimitra maoutsa" width="30%" height="30%">
   <img src="images/scheduling.png" alt="schematic of the two variants of the scheduling algorithms by dimitra maoutsa" width="30%" height="30%">
   </div>
   



[_](https://gitlab.com/dimitra-maoutsa/dimitra-maoutsa.gitlab.io/-/blob/master/googled9cc947ded865a6d.html)

[_](<a rel="me" href="https://scholar.social/@_dim_ma_">m</a>)













