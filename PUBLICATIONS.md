- Casadiego\*, J.; **Maoutsa\*, D.**; Timme, M.  
    [_**Inferring network connectivity from event timing patterns.**_](https://gitlab.com/di.ma/Connectivity_from_event_timing_patterns/-/blob/master/PhysRevLett.121.054101.pdf)  
    **Physical Review Letters**, 2018.  

- **Maoutsa, Dimitra**; Reich, Sebastian; Opper, Manfred.  
    [_**Interacting particle solutions of Fokker–Planck equations through gradient–log–density estimation.**_](https://www.mdpi.com/1099-4300/22/8/802/htm)   
 **Entropy**, 2020. **(Editor's choice)**

- Rémi Gau\*, Stephanie Noble\*, Katja Heuer\*, Katherine L Bottenhorn\*, Isil P Bilgin\*, Yu-Fang Yang\*, Julia M Huntenburg\*, Johanna MM Bayer\*, Richard AI Bethlehem\*, et al. (The Brainhack Community)  
    [_**Brainhack: Developing a culture of open, inclusive, community-driven neuroscience.**_](https://www.sciencedirect.com/science/article/pii/S0896627321002312)  
    **Neuron**, 2021.

- **Maoutsa, Dimitra**; Opper, Manfred.  
    [_**Deterministic particle flows for constraining SDEs.**_](https://arxiv.org/abs/2110.13020)   
    **NeurIPS workshop Machine Learning for the physical sciences**, 2021.

- **Maoutsa, Dimitra**; Opper, Manfred.  
    [_**Deterministic particle flows for constraining stochastic nonlinear systems.**_ ](https://arxiv.org/abs/2112.05735)  
    **Physical Review Research**, 2022. 

- **Maoutsa, Dimitra**.  
    [_**Geometric path augmentation for inference of sparsely observed stochastic nonlinear systems.**_](https://nips.cc/virtual/2022/workshop/49979)   
    **NeurIPS workshop Machine Learning for the physical sciences**, 2022.

- **Maoutsa, Dimitra**.  
    [_**Geometric constraints improve inference of sparsely observed stochastic dynamics.**_](https://openreview.net/forum?id=ppHJfYzL96)   
    **ICLR workshop Physics for Machine Learning**, 2023.

- **Maoutsa, Dimitra**.  
    [_**"Deterministic particle flows for stochastic nonlinear systems: Simulation, Control, and Inference"**_](https://www.tu.berlin/en/eecs/event-details/events/event/0186e92c-7aa6-7fb2-b57d-5878e18ba913)   
    **PhD Thesis - TU Berlin _( summa cum laude )_**, _(awaiting publication)_, 2023


- **Maoutsa, Dimitra**. 
    _**Geometric path augmentation for inference of sparsely observed stochastic systems.**_   
    _(journal article - in preparation)_

